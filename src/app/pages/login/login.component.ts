import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,

  ) { }

  ngOnInit(): void {
    this.fg = this.fb.group({
      email: [],
      password: []
    });
  }

  submit() {
    this.http.post<string>(environment.api_uri + '/security/login', this.fg.value)
      .subscribe(token => {
        localStorage.setItem('NGTOKEN', token);
        this.router.navigateByUrl('/add-animal');
      })
  }

}
