import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.scss']
})
export class AddAnimalComponent implements OnInit {

  fg!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,

  ) { }

  ngOnInit(): void {
    this.fg = this.fb.group({
      name: [],
    });
  }

  submit() {
    this.http.post<string>(environment.api_uri + '/animal', this.fg.value)
      .subscribe(token => {
        alert('OK')
      })
  }

}
